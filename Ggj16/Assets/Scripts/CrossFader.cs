﻿using UnityEngine;
using System.Collections;

public class CrossFader : MonoInstance<CrossFader> {

	[SerializeField]private AudioClip MenuTrack;

	[SerializeField]private AudioSource m_source1;

	float m_audioVolume1 = 1f;

	//public bool m_bCanFade = false;

	public bool m_bCanFadeIn = false;
	public bool m_bCanFadeOut = false;

	public bool m_WillStartOnAwake = false;

	public enum FadeType{
		fadeIn,
		fadeOut
	}

	// Use this for initialization
	void Start () {
		m_source1.clip = MenuTrack;

		if(m_WillStartOnAwake)
			PlayFader(FadeType.fadeIn);
	}

	public void PlayFader(FadeType p_type)
	{
		//m_bCanFade = true;
		if(p_type == FadeType.fadeIn){
			m_source1.Play();
			m_bCanFadeIn = true;
		}
		else
			m_bCanFadeOut = true;
	}

	public void StopFader()
	{
		m_bCanFadeOut = false;
		m_bCanFadeIn = false;
		//m_bCanFade = false;
		m_source1.Stop();
	}

	// Update is called once per frame
	void Update () {
		//if(!m_bCanFade)
			//return;
		
		if(m_bCanFadeIn){
			FadeIn();
			if(m_audioVolume1 >= 1)
			{
				m_bCanFadeIn = false;
			}
		}
		if(m_bCanFadeOut)
		{
			FadeOut();
			if(m_audioVolume1 <= 0)
			{
				StopFader();
			}
		}
	}

	public void FadeIn()
	{
		if(m_audioVolume1 < 1){
			m_audioVolume1 += 0.5f * Time.deltaTime;
			m_source1.volume = m_audioVolume1;
		}
	}

	public void FadeOut()
	{
		if(m_audioVolume1 >= 0){
			m_audioVolume1 -= 0.5f * Time.deltaTime;
			m_source1.volume = m_audioVolume1;
		}
	}
}
