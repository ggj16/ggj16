﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class CameraController : MonoBehaviour {

	private VignetteAndChromaticAberration m_aberration;
	private BloomOptimized m_bloom;

	private void Start()
	{
		Application.targetFrameRate = 60;

		#if !UNITY_EDITOR && !UNITY_WEBGL
		Cursor.visible = false;
		#endif

		m_aberration = GetComponent<VignetteAndChromaticAberration>();
		m_bloom = GetComponent<BloomOptimized>();

		//if( Application.platform == RuntimePlatform.WebGLPlayer ) {
		QualitySettings.SetQualityLevel( PlayerPrefs.GetInt( "KeyQuality", 0 ), true );			
		//}

		UpdateQuality();
	}

	private void Update()
	{
		if( Input.GetKey( KeyCode.Alpha1 ) ) {
			PlayerPrefs.SetInt( "KeyQuality", 1 );
			QualitySettings.SetQualityLevel( 1, true );
			UpdateQuality();
		}
		else if( Input.GetKey( KeyCode.Alpha2 ) ) {
			PlayerPrefs.SetInt( "KeyQuality", 0 );
			QualitySettings.SetQualityLevel( 0, true );
			UpdateQuality();
		}
	}

	private void UpdateQuality()
	{
		bool bHigh = System.Convert.ToBoolean( QualitySettings.GetQualityLevel() );
		m_aberration.enabled = false;
		m_bloom.enabled = bHigh;
	}
}
