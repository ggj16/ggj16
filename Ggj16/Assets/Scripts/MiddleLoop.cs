﻿using UnityEngine;
using System.Collections;

public class MiddleLoop : MonoInstance<MiddleLoop> {

	[SerializeField]private AudioClip otherClip;
	AudioSource audio;
	[SerializeField]private AudioSource otherSource;
	public bool m_willPlay = false;

	public bool WillPlay { 
		set{m_willPlay = value;}
		get { return m_willPlay; } 
	}

	void Start() {
		audio = GetComponent<AudioSource>();
	}

	void Update() {
		if (!m_willPlay)
			return;
		if (!audio.isPlaying) {
			//audio.clip = otherClip;
			//audio.Play();
			//audio.loop = true;
			otherSource.Play();
			m_willPlay = false;
		}
	}
}
