﻿using UnityEngine;
using System.Collections;

public class GaugeManager : MonoInstance<GaugeManager> {

	private GaugeController[] m_controllers;

	protected override void Awake() {
		base.Awake();
		m_controllers = GetComponentsInChildren<GaugeController>();
	}

	private void Start() {
		foreach( GaugeController controller in m_controllers ) {
			//controller.gameObject.SetActive( false );
			controller.transform.localScale = Vector3.zero;
		}
	}

	public void SetControllersPercentage( float p_percentage )
	{
		p_percentage = Mathf.Clamp01( p_percentage );

		//Debug.Log( m_controllers.Length );

		foreach( GaugeController controller in m_controllers ) {
			controller.SetPercentage( p_percentage );
		}
	}

	public void AnimateStart()
	{
		foreach( GaugeController controller in m_controllers ) {
			//controller.gameObject.SetActive( true );

			controller.transform.localScale = Vector3.zero;

			float duration = Random.Range( 0.5f, 1.5f );
			float delay = Random.Range( 0f, 0.5f );

			iTween.ScaleTo( controller.gameObject,
				iTween.Hash( "scale", Vector3.one, "time", duration, "delay", delay,
					"easeType", iTween.EaseType.easeOutQuint ));
		}
	}

}
