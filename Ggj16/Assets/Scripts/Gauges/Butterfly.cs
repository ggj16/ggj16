﻿using UnityEngine;
using System.Collections;

public class Butterfly : GaugeObject {

	protected override void Awake()
	{
		GetComponent<Animator>().speed = Random.Range( 0.85f, 1.25f );

		transform.localPosition += (Vector3)(Helper.RandomNormalizeVector2D() * Random.Range( 0.15f, 0.5f ));
		transform.localRotation *= Quaternion.Euler( 0, 0, Random.Range( -1, 1 ) * 10 );
		transform.localScale = Vector3.one * Random.Range( 0.35f, 0.75f );

		GetComponent<SpriteRenderer>().flipX = Helper.RandomBoolean();

		base.Awake();

		iTween.ValueTo( gameObject,
			iTween.Hash( "from", 0f, "to", Random.Range( 1f, 2f ), "time", Random.Range( 8f, 12f ),
				"easeType", iTween.EaseType.easeInOutExpo, "looptype", iTween.LoopType.pingPong, "delay", Random.Range( 1f, 2f ),
				"onupdate", "TweenPosY", "onupdatetarget", gameObject ));
	}
}
