﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GaugeObject : MonoBehaviour {

	[SerializeField] protected List<Sprite> m_seeds = null;

	protected Vector3 m_origPos = Vector3.zero;

	protected virtual void Awake()
	{
		m_origPos = transform.localPosition;		
	}

	protected void RandomizeSprite()
	{
		GetComponent<SpriteRenderer>().sprite = m_seeds[Random.Range(0, m_seeds.Count)];
	}

	public void RemoveFromWorld()
	{
		iTween.Stop( gameObject, false );

		float duration = Random.Range( 0.5f, 2f );

		Animator animator = GetComponent<Animator>();

		if( animator != null ) {
			animator.speed = 0;
		}

		iTween.Stop( gameObject );


		iTween.ScaleTo( gameObject,
			iTween.Hash( "scale", Vector3.one * Random.Range( 1f, 3f ), "time", duration,
				"easeType", iTween.EaseType.easeOutSine, "oncompletetarget", gameObject, "oncomplete", "DestroyObj" ));

		iTween.FadeTo( gameObject,
			iTween.Hash( "alpha", 0, "time", duration,
				"easeType", iTween.EaseType.easeOutCubic ));	
	}

	private void DestroyObj()
	{
		Destroy( gameObject );
	}

	protected void TweenPosX( float p_value )
	{
		transform.localPosition = m_origPos + new Vector3( p_value, 0, 0 );
	}

	protected void TweenPosY( float p_value )
	{
		transform.localPosition = m_origPos + new Vector3( 0, p_value, 0 );
	}
}
