﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GaugeController : MonoBehaviour {

	[SerializeField] protected bool m_bWillSpawn = false;
	[SerializeField] protected GameObject m_obj = null;
	[SerializeField] protected int m_count = 0;
	[SerializeField] protected float m_minRadius = 3.5f;
	[SerializeField] protected float m_maxRadius = 6.5f;
	[SerializeField] protected bool m_bIsUp = false;

	protected List<GameObject> m_objects = null;

	private int m_originalCount = 0;

	private void Awake()
	{
		m_objects = new List<GameObject>();

		if( m_bWillSpawn ) {
			Spawn( m_count, m_bIsUp );
			m_obj.SetActive( false );
		}

		foreach( Transform t in transform ) {
			if( t.gameObject.activeSelf ) {
				m_objects.Add( t.gameObject );
			}
		}

		m_objects = Helper.Randomize<GameObject>( m_objects );

		m_originalCount = m_objects.Count;
	}

	private void Spawn( int p_count, bool p_bUp )
	{
		int count = p_count;
		count -= 1;
		float allow = 180f / (float)count * ( p_bUp ? -1 : 1 );

		for( int i = 0 ; i < count; i++ ) {
			//Debug.DrawRay( Vector3.zero, Quaternion.Euler( 0, 0, -i * 9 ) * Vector3.left, Color.red, 10 );

			Vector3 pos = Quaternion.Euler( 0, 0, i * allow + allow * 0.5f ) * Vector3.left * Random.Range( 3.5f, m_maxRadius );

			Object obj = Instantiate( m_obj, pos, Quaternion.identity );
			GameObject gObj = obj as GameObject;
			gObj.transform.SetParent( transform );
			//gObj.transform.localPosition = pos;
			gObj.transform.localScale = Vector3.one;
		}		
	}
		
	public void SetPercentage( float p_percent )
	{
		int count = Mathf.FloorToInt( m_originalCount * p_percent );

		int toDestroyCount = m_objects.Count - count;

		if( toDestroyCount <= 0 ) {
			return;
		}

		for( int i = toDestroyCount; i > 0; i-- ) {

			m_objects[ 0 ].GetComponent<GaugeObject>().RemoveFromWorld();
			m_objects.RemoveAt( 0 );
		}

		//Debug.Log( p_percent + " " + toDestroyCount );
	}
}
