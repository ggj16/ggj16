﻿using UnityEngine;
using System.Collections;

public class Star : GaugeObject {
	
	protected override void Awake()
	{
		RandomizeSprite();

		transform.localScale = Vector3.one * Random.Range( 0f, 0.35f );

		base.Awake();

		iTween.ScaleTo( gameObject,
			iTween.Hash( "scale", Vector3.one * Random.Range( 0.5f, 1f ), "time", Random.Range( 0.15f, 0.8f ),
				"easeType", iTween.EaseType.easeInOutSine, "looptype", iTween.LoopType.pingPong, "delay", Random.Range( 0.5f, 1f ) ));
	}
}
