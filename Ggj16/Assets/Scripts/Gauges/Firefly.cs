﻿using UnityEngine;
using System.Collections;

public class Firefly : GaugeObject {

	protected override void Awake()
	{
		GetComponent<Animator>().speed = Random.Range( 0.85f, 1.25f );

		transform.localPosition += (Vector3)(Helper.RandomNormalizeVector2D() * Random.Range( 0.15f, 0.5f ));
		transform.localScale = Vector3.one * Random.Range( 0.35f, 0.65f );
		transform.localRotation = Helper.RandomRotation2D();

		base.Awake();

		/*
		GetComponent<SpriteRenderer>().flipX = Helper.RandomBoolean();

		iTween.MoveBy( gameObject,
			iTween.Hash( "x", Random.Range( -1f, 1f ) * 2, "time", Random.Range( 8f, 15f ),
				"easeType", iTween.EaseType.easeInOutSine, "looptype", iTween.LoopType.pingPong, 
				"delay", Random.Range( 1f, 3f ) ));*/
	}
}
