﻿using UnityEngine;
using System.Collections;

public class Cloud : GaugeObject {

	protected override void Awake()
	{
		RandomizeSprite();

		transform.localScale = Vector3.one * Random.Range( 0.85f, 1.5f );

		GetComponent<SpriteRenderer>().flipX = Helper.RandomBoolean();

		base.Awake();

		iTween.ValueTo( gameObject,
			iTween.Hash( "from", 0f, "to", Random.Range( -1f, 1f ) * 2, "time", Random.Range( 8f, 15f ),
				"easeType", iTween.EaseType.easeInOutSine, "looptype", iTween.LoopType.pingPong, "delay", Random.Range( 1f, 3f ),
				"onupdate", "TweenPosX", "onupdatetarget", gameObject ));
	}
}
