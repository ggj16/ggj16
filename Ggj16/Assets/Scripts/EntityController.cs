﻿using UnityEngine;
using System.Collections;

public class EntityController : MonoBehaviour {

	private Animator m_animator = null;

	private void Awake () {
		m_animator = GetComponent<Animator>();
		m_animator.speed = Random.Range( 0.4f, 0.6f );
	}

	public void AnimateIdle () {
		m_animator.speed = 1f;
	}

	public void AnimateHappy () {
		m_animator.SetTrigger( "triggerHappy" );
	}
}
