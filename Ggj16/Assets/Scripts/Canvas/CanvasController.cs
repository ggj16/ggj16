﻿using UnityEngine;
using System.Collections;

public class CanvasController<T> : MonoInstance<T> where T : MonoBehaviour
{
	protected override void Awake()
	{
		base.Awake();
	}

	public virtual void Show( bool p_bShow )
	{
		gameObject.SetActive( p_bShow );
	}
}
