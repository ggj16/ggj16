﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoInstance<GameController> {

	public enum menuState
	{
		pressAnyState,
		tutState
	}

	[SerializeField] private RectTransform m_panel = null;
	[SerializeField] private QuicktimePlayHold m_play = null;
	[SerializeField] private CrossFader m_fadeOut = null;
	[SerializeField] private CrossFader m_fadeIn = null;

	private menuState m_currState = menuState.pressAnyState;

	protected override void Awake ()
	{
		m_play.AnimatePressAny();

		base.Awake ();
	}

	public void OnClickPlay()
	{
		m_fadeOut.PlayFader(CrossFader.FadeType.fadeOut);
		m_fadeIn.PlayFader(CrossFader.FadeType.fadeIn);
		m_fadeIn.GetComponent<MiddleLoop>().WillPlay = true;

		DayController.Instance.StartGame();
        QuicktimeSpawner.Instance.StartGame();
		m_panel.gameObject.SetActive( false );
	}

	public void OnPressAnyKey()
	{
		SfxManager.Instance.Play( SfxManager.SfxClip.Clip1, 1, C.PitchVal );
		m_play.ShowObject();
		m_currState = menuState.tutState;
	}

	void Update()
	{
		switch(m_currState)
		{
		case menuState.pressAnyState:
			if(Input.anyKey)
			{
				OnPressAnyKey();
			}
			break;
		case menuState.tutState:
			break;
		}
	}
}
