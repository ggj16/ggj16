﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public abstract class MonoInstance<T> : MonoBehaviour where T : MonoBehaviour
{
	protected static T s_instance = null;
	public static T Instance { get { return s_instance; } }

	protected virtual void Awake()
	{
		s_instance = this as T;
	}

	protected virtual void OnDestroy()
	{
		s_instance = null;
	}
}
