﻿using UnityEngine;
using System.Collections;

public class InputHandler : MonoInstance<InputHandler> {

	public bool keyIsPressed = false;
	public bool keyIsHolding = false;
	public bool CanHold = false; //Will be called if 2 players nee to hold a button
	//private C.Controls inputKey = C.Controls.Invalid;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	/*void Update () {
		if(!CanHold)
		{
			//------ Button Press
			//Player 1
			if(Input.GetButtonDown(C.P1Up) && inputKey == C.Controls.P1Up)
				Debug.Log("P1 - Up");
			else if(Input.GetButtonDown(C.P1Down) && inputKey == C.Controls.P1Down)
				Debug.Log("P1 - Down");
			else if(Input.GetButtonDown(C.P1Left) && inputKey == C.Controls.P1Left)
				Debug.Log("P1 - Left");
			else if(Input.GetButtonDown(C.P1Right) && inputKey == C.Controls.P1Right)
				Debug.Log("P1 - Right");
			else{
				keyIsPressed = false;
			}
			//Player 2
			if(Input.GetButtonDown(C.P2Up) && inputKey == C.Controls.P2Up)
				Debug.Log("P2 - Up");
			else if(Input.GetButtonDown(C.P2Down) && inputKey == C.Controls.P2Down)
				Debug.Log("P2 - Down");
			else if(Input.GetButtonDown(C.P2Left) && inputKey == C.Controls.P2Left)
				Debug.Log("P2 - Left");
			else if(Input.GetButtonDown(C.P2Right) && inputKey == C.Controls.P2Right)
				Debug.Log("P2 - Right");
			else{
				keyIsPressed = false;
			}
		}
		else{
			//------ Button Hold
			if(Input.GetButton(C.P1Up))
				Debug.Log("P1 - Up");
			else if(Input.GetButton(C.P1Down))
				Debug.Log("P1 - Down");
			else if(Input.GetButton(C.P1Left))
				Debug.Log("P1 - Left");
			else if(Input.GetButton(C.P1Right))
				Debug.Log("P1 - Right");
			else{
				keyIsHolding = false;
			}
			if(Input.GetButton(C.P2Up))
				Debug.Log("P2 - Up");
			else if(Input.GetButton(C.P2Down))
				Debug.Log("P2 - Down");
			else if(Input.GetButton(C.P2Left))
				Debug.Log("P2 - Left");
			else if(Input.GetButton(C.P2Right))
				Debug.Log("P2 - Right");
			else{
				keyIsPressed = false;
			}
		}
	}*/
	/*
	 * Call this on Quick Time event spawner with corresponding input key. 
	 * If The player succesfully press the key, keyIsPressed will be true. 
	 */
	public void OnKeyPress(C.Controls p_control)
	{
		/*switch(p_control)
		{
		//---------Player 1
		case C.Controls.P1Up:
			inputKey = C.Controls.P1Up;
			break;
		case C.Controls.P1Down:
			inputKey = C.Controls.P1Down;
			break;
		case C.Controls.P1Left:
			inputKey = C.Controls.P1Left;
			break;
		case C.Controls.P1Right:
			inputKey = C.Controls.P1Right;
			break;
		//---------Player 2
		case C.Controls.P2Up:
			inputKey = C.Controls.P2Up;
			break;
		case C.Controls.P2Down:
			inputKey = C.Controls.P2Down;
			break;
		case C.Controls.P2Left:
			inputKey = C.Controls.P2Left;
			break;
		case C.Controls.P2Right:
			inputKey = C.Controls.P2Right;
			break;
		}*/
	}

	public void OnKeyHold()
	{
		
	}
}
