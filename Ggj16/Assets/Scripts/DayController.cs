﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DayController : MonoInstance<DayController> {

	[SerializeField] private RectTransform m_panel = null;
	[SerializeField] private Text m_txtDay = null;
	[SerializeField] private CrossFader m_fadeOut = null;

    private Animator m_animator = null;

	private int m_day = 1;

	private bool m_bInTransit = false;
	public bool InTransit { get { return m_bInTransit; } }
    public int CurrentDay { get { return m_day; } }

    protected override void Awake()
	{
		base.Awake();

		m_panel.anchoredPosition = new Vector2(0, 150 );
		iTween.Init( m_panel.gameObject );

		m_animator = GetComponent<Animator>();

		RefreshDayText();
	}

	private void RefreshDayText()
	{
		iTween.PunchScale( m_txtDay.transform.parent.gameObject, new Vector3( 0.15f, 0.15f, 0 ), 1f );

        m_txtDay.text = "" + m_day;
	}

	public void StartGame()
	{
		/*
		iTween.MoveTo( m_panel.gameObject,
			iTween.Hash( "y", 0, "time", 1.0f, "easeType", iTween.EaseType.easeOutSine ));
		*/
		iTween.ValueTo( m_panel.gameObject,
			iTween.Hash( "from", 150f, "to", 0f, "time", 1.0f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenTop", "onupdatetarget", gameObject ));

		GaugeManager.Instance.AnimateStart();

		WorldController.Instance.AnimateEntityIdle();

		StartNewDay();
    }

	public void TweenTop( float p_value )
	{
		m_panel.anchoredPosition = new Vector2( 0, p_value );
	}

	public void StartNewDay()
	{
		m_bInTransit = true;
		m_animator.SetTrigger( "triggerDeplete" );

		RefreshDayText();
        m_animator.speed = 0.2f + (0.75f * ((float)m_day / (float)C.DayMax));

        Debug.Log( "Start New day" );
	}

	public void EndDay()
    {
        QuicktimeSpawner.Instance.StopSpawn();
        m_bInTransit = false;

		Debug.Log( "End Day" );

		m_animator.speed = 0;

		GaugeManager.Instance.SetControllersPercentage(1 - ((float)( m_day ) / ( (float)C.DayMax - 1 ) ));

        m_day++;


		if( m_day >= C.DayMax ) {
			m_fadeOut.PlayFader(CrossFader.FadeType.fadeOut);

			WorldController.Instance.FadeInNewWorld();

			iTween.ValueTo( m_panel.gameObject,
				iTween.Hash( "from", 0, "to", 150f, "time", 1.0f, "easeType", iTween.EaseType.easeOutExpo,
					"onupdate", "TweenTop", "onupdatetarget", gameObject ));

		}
		else {
			Invoke( "DelayedEndDay", 3.0f );
		}
    }

	private void DelayedEndDay()
    {
        m_animator.speed = 1;
        Debug.Log("speed : " + m_animator.speed);
        m_animator.SetTrigger( "triggerReset" );		
		StartNewDay();
        QuicktimeSpawner.Instance.UpdateProgress();
    }

	public void OnAnimBarDepleted()
	{
        //return;
		m_bInTransit = false;		
		Debug.Log( "Game Over" );

		WorldController.Instance.ShakeWorld();
	}

    public void OnInputError()
    {
        m_animator.speed = 0;
        Invoke("DelayedResume", 3.0f);
    }

    private void DelayedResume()
    {
        m_animator.speed = 0.1f + (0.6f * ((float)m_day / (float)C.DayMax));
        QuicktimeSpawner.Instance.UpdateProgress();
        QuicktimeSpawner.Instance.InErrorSequence = false;
    }
}
