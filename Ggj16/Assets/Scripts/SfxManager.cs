﻿using UnityEngine;
using System.Collections;

public class SfxManager : MonoInstance<SfxManager>
{
	public enum SfxClip
	{
		Clip1,
		Clip2,
		Clip3,
		Clip4
	}

	private int SOURCE_COUNT = 3;

	private AudioSource[] m_listAudioSource;

	[SerializeField] private AudioClip[] m_clips;

	protected override void Awake()
	{
		base.Awake();

		m_listAudioSource = new AudioSource[SOURCE_COUNT];

		for( int i = 0; i < SOURCE_COUNT; i++ ) {
			m_listAudioSource [i] = gameObject.AddComponent<AudioSource> ();
		}
	}

	public void Play( SfxClip p_clip, float p_volume = 1.0f, float p_pitchVariance = 0.0f )
	{		
		for( int i = 0; i < SOURCE_COUNT; i++ )
		{
			AudioSource audioSource = m_listAudioSource[i];

			/*
			if( audioSource.isPlaying && audioSource.clip == m_clips[(int)p_clip]  )
			{
				return;
			}*/

			// Look for available Audio Source.
			if( !audioSource.isPlaying )
			{
				audioSource.clip = m_clips[(int)p_clip];
				audioSource.Play();
				audioSource.volume = p_volume;
				audioSource.pitch = 1.0f + Random.Range( -p_pitchVariance, p_pitchVariance );				
				break;
			}
		}
	}

	public static void PlayClip1()
	{
		SfxManager.Instance.Play( SfxClip.Clip1 );
	}

	public static void PlayClip2()
	{
		SfxManager.Instance.Play( SfxClip.Clip2 );
	}
}
