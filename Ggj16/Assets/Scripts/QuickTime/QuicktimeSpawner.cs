﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuicktimeSpawner : MonoInstance<QuicktimeSpawner> {

    public string[] m_inputKeys = {C.P1Up, C.P1Down, C.P1Left, C.P1Right,
        C.P2Up, C.P2Down, C.P2Left, C.P2Right};

    public GameObject       m_quicktimeObject;
    public GameObject       m_quicktimeHoldObject;
    public Transform        m_pointSpawner;
    public List<Transform>  m_points = new List<Transform>();
    private string          m_currentSide = "";

    public List<C.Controls> m_lightButtons = new List<C.Controls>();
    public List<C.Controls> m_darkButtons = new List<C.Controls>();

    public List<GameObject> m_lightButtonsObj = new List<GameObject>();
    public List<GameObject> m_darkButtonsObj = new List<GameObject>();

    //---level progression variables
    public float m_timeInterval;
    public int m_LDchance;
    public float m_timeToDisappear;
    //---initial level progression variables
    public float m_timeInterval_;
    public int   m_LDchance_;
    public float m_timeToDisappear_;

	private bool m_bInWrongSequence;

    protected override void Awake()
    {
        base.Awake();
    }

    // Use this for initialization
    void Start ()
    {
        m_timeInterval_ = m_timeInterval;
        m_LDchance_ = m_LDchance;
        m_timeToDisappear_ = m_timeToDisappear;
    }

    public void StartGame()
    {
        foreach (Transform trans in m_pointSpawner)
        {
            m_points.Add(trans);
        }
        StartSpawnSequence();
        StartSpawnMidSequence();
    }

    public void StopSpawn()
    {
		CancelInvoke("StartSpawnSequence");
		CancelInvoke("SpawnObject");
		CancelInvoke("StartSpawnMidSequence");
		DestroyAllSpawn();
    }

    bool RandomChance(int percentage)
    {
        int chance = Random.Range(0, 100);
        if (chance <= percentage) { return true; }
        else { return false; }
    }

    public void UpdateProgress()
    {
        m_timeInterval = m_timeInterval_ - (DayController.Instance.CurrentDay * 0.125f);
        m_LDchance = m_LDchance_ + (DayController.Instance.CurrentDay * 3);
        m_timeToDisappear = m_timeToDisappear_ - (DayController.Instance.CurrentDay * 0.1f);

        //---limit
        if (m_timeInterval < 0.5f) { m_timeInterval = 0.5f; }
        if (m_LDchance > 89  ) { m_LDchance = 89; }
        if (m_timeToDisappear < 1.7f) { m_timeToDisappear = 1.7f; }
        //---
        StartSpawnSequence();
        StartSpawnMidSequence();
    }

    public void StartSpawnMidSequence()
    {
		Invoke("SpawnMidSequence", 30 - ( DayController.Instance.CurrentDay * 3 ));
    }

    void SpawnMidSequence()
    {
        int random = Random.Range(0, m_points.Count);
        if (m_points[random].childCount == 0)
		{
			if ( !DayController.Instance.InTransit || m_bInWrongSequence)
			{
				CancelInvoke("SpawnMidSequence");
				Invoke("SpawnMidSequence", 30 - ( DayController.Instance.CurrentDay * 3 ));
				return;
			}
            GameObject button = (GameObject)Instantiate(m_quicktimeHoldObject, Vector3.zero, Quaternion.identity);
            button.transform.SetParent(m_points[random]);
            button.transform.localPosition = Vector2.zero;
            button.transform.localScale = Vector2.one;

            C.Controls control = randDirection();
            C.Sides side = getSide(m_currentSide);

            List<C.Controls> controls = new List<C.Controls>();
            if (side == C.Sides.Light)
                controls = m_lightButtons;
            else
                controls = m_darkButtons;

//            bool added = false;
//            int tries = 0;
//            while (!added || tries >= 4)
//            {
//                if (controls.Contains(control))
//                {
//                    control = randDirection();
//                    tries++;
//                }
//                else
//                {
//                    controls.Add(control);
//                    added = true;
//                }
//            }
            
			button.GetComponent<QuicktimeHold>().OnSpawn(control, side, m_timeToDisappear + 3.5f);
			//yield return new WaitForSeconds(m_timeInterval);
			CancelInvoke("SpawnMidSequence");
			Invoke("SpawnMidSequence", 30 - ( DayController.Instance.CurrentDay * 3 ));
        }
        else
		{
			CancelInvoke("SpawnMidSequence");
			SpawnMidSequence();
        }
    }

    public void StartSpawnSequence()
    {
		Invoke("SpawnSequence", m_timeInterval);
    }

	void SpawnSequence()
    {
		SpawnObject();
        if (RandomChance(m_LDchance))
		{
			Invoke("SpawnObject", Random.Range(0, 1.5f));
        }
    }

	void SpawnObject()
    {
        int random = Random.Range(0, m_points.Count);
        if (m_points[random].childCount == 0 && m_points[random].tag != m_currentSide)
        {
			if ( !DayController.Instance.InTransit || m_bInWrongSequence)
			{
				CancelInvoke("StartSpawnSequence");
				Invoke("StartSpawnSequence", m_timeInterval);
				return;
			}
            m_currentSide = m_points[random].tag;
            GameObject button = (GameObject)Instantiate(m_quicktimeObject, Vector3.zero, Quaternion.identity);
            button.transform.SetParent(m_points[random]);
            button.transform.localPosition = Vector2.zero;
            button.transform.localScale = Vector2.one;

            C.Controls control = randDirection();
            C.Sides side = getSide(m_currentSide);

            List<C.Controls> controls = new List<C.Controls>();
            if (side == C.Sides.Light)
            {
                m_lightButtonsObj.Add(button);
                controls = m_lightButtons;
            }
            else
            {
                m_darkButtonsObj.Add(button);
                controls = m_darkButtons;
            }

//            bool added = false;
//            int tries = 0;
//            while (!added || tries >= 4)
//            {
//                if ( controls.Contains(control) )
//                {
//                    control = randDirection();
//                    tries++;
//                }
//                else
//                {
//                    controls.Add(control);
//                    added = true;
//                }
//            }

			button.GetComponent<QuicktimeTap>().OnSpawn(control, side, m_timeToDisappear);

			CancelInvoke("StartSpawnSequence");
			Invoke("StartSpawnSequence", m_timeInterval);
        }
        else
		{
			CancelInvoke("SpawnObject");
			SpawnObject();
        }
    }

    public bool InErrorSequence
    {
        get { return m_bInWrongSequence; }
        set { m_bInWrongSequence = value; }
    }

    C.Sides getSide (string p_tag)
    {
        C.Sides side_ = C.Sides.None;
        if (p_tag == "L" ) { side_ = C.Sides.Light; }
        else if (p_tag == "D" ) { side_ = C.Sides.Dark; }
        return side_;
    }

    C.Controls randDirection ()
    {
        int random = Random.Range(0, 4);
        Debug.Log(random);
        C.Controls dir = C.Controls.Invalid;
        switch (random)
        {
            case 0:
                dir = C.Controls.Up;
                break;
            case 1:
                dir = C.Controls.Down;
                break;
            case 2:
                dir = C.Controls.Left;
                break;
            case 3:
                dir = C.Controls.Right;
                break;
            default:
                dir = C.Controls.Invalid;
                break;
        }

        return dir;
    }

    public void ClearButtonList(C.Sides p_side, C.Controls p_control, GameObject p_obj)
    {
        List<C.Controls> controls = new List<C.Controls>();
        if (p_side == C.Sides.Light)
        {
            controls = m_lightButtons;
            m_lightButtonsObj.Remove(p_obj);
        }
        else
        {
            controls = m_darkButtons;
            m_darkButtonsObj.Remove(p_obj);
        }

        controls.Remove(p_control);
    }

    public bool CheckIfHoldExists()
    {
        foreach (Transform trans in m_pointSpawner)
        {
            if ( trans.childCount != 0)
            {
                if (trans.GetComponentInChildren<QuicktimeHold>() != null || trans.GetComponentInChildren<QuicktimeHold>() != null)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void DestroyAllSpawn()
    {
        foreach (Transform trans in m_pointSpawner)
        {
            if (trans.childCount != 0)
            {
                if (trans.GetComponentInChildren<QuicktimeObject>() != null)
                {
                    trans.GetComponentInChildren<QuicktimeObject>().OnAnimSuccess();
                }
            }
        }
    }

    public void WrongInputSequence()
    {
        if (m_bInWrongSequence) { return; }
		m_bInWrongSequence = true;
		StopSpawn();
        foreach (Transform trans in m_pointSpawner)
        {
            if (trans.childCount != 0)
            {
                if (trans.GetComponentInChildren<QuicktimeObject>() != null)
                {
                    trans.GetComponentInChildren<QuicktimeObject>().OnAnimSuccess();
                }
            }
        }
        SfxManager.Instance.Play(SfxManager.SfxClip.Clip2, 1, C.PitchVal);
        WorldController.Instance.DeductWorldRotation();
        DayController.Instance.OnInputError();
    }

    void Update()
    {
        /*
        int min = 0;
        int max = 0;

        if (m_side == C.Sides.Light)
        {
            min = 0;
            max = 3;
        }
        else
        {
            min = 4;
            max = 7;
        }
        //--
        */
        if ( !DayController.Instance.InTransit)
        {
			return;
		}

        for (int i = 0; i < m_inputKeys.Length; i++)
        {
            if (Input.GetButtonDown(m_inputKeys[i]))
            {
                foreach (GameObject qtTap in m_lightButtonsObj)
                {
					if (qtTap && qtTap.GetComponent<QuicktimeTap>() != null)
                    {
                        QuicktimeTap tap = qtTap.GetComponent<QuicktimeTap>();
                        if (i == (int)tap.InputKey && !tap.WasTap)
                        {
                            tap.OnTap();
                            return;
                        }
                    }
                }
                foreach (GameObject qtTap in m_darkButtonsObj)
                {
					if (qtTap && qtTap.GetComponent<QuicktimeTap>() != null)
                    {
                        QuicktimeTap tap = qtTap.GetComponent<QuicktimeTap>();
                        if (i == (int)tap.InputKey && !tap.WasTap)
                        {
                            tap.OnTap();
                            return;
                        }
                    }
                }
                if ( CheckIfHoldExists() ) { return; }
                WrongInputSequence();
            }
        }
    }
}
