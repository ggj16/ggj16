﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class QuicktimeTap : QuicktimeObject {
	
	[SerializeField]private Sprite m_lightSymbol;
	[SerializeField]private Sprite m_darkSymbol;

	private C.Controls m_inputKey;
	private C.Sides m_side;

	private bool m_bWasTap = false;

	// Use this for initialization
	void Start () {
		//OnSpawn(C.Controls.Right, C.Sides.Dark, 5f); //Dont commit this bien!
	}

	protected override void UpdateObj ()
	{
		/*
		// Bien! Change this, this is ugly code :(
		int min = 0;
		int max = 0;

		if(m_side == C.Sides.Light){
			min = 0;
			max = 3;
		}
		else{
			min = 4;
			max = 7;
		}
		//--

		for(int i = min; i <= max; i++)
		{
			if(Input.GetButtonDown(m_inputKeys[i]))
			{
				Debug.Log(i);
				if(i == (int)m_inputKey && !m_bWasTap)
					OnTap();
                else
                {
                    OnWrongTap();
                }
			}
		}

		base.UpdateObj ();*/
	}

	public override void OnSpawn(C.Controls p_input, C.Sides p_side, float p_time)
	{
		if(p_side == C.Sides.Light){
			m_currSpr = m_lightSymbol;
			m_offset = 0;
		}
		else{
			m_currSpr = m_darkSymbol;
			m_offset = 4;
		}

		m_side = p_side;
		m_inputKey = p_input + m_offset;

		base.OnSpawn(p_input, p_side, p_time);
	}

	protected override void OnDestroy ()
	{
		iTween.Stop(gameObject);
        if ( QuicktimeSpawner.Instance )
        {
            QuicktimeSpawner.Instance.ClearButtonList(m_side, m_inputKey - m_offset, this.gameObject);
        }
		base.OnDestroy ();
	}

	public void OnTap()
	{
		m_bWasTap = true;

		OnAnimSuccess();

		SfxManager.Instance.Play( SfxManager.SfxClip.Clip1, 1, C.PitchVal );

		WorldController.Instance.AddWorldRotation();

		WorldController.Instance.AnimateEntityHappy( ( m_side == C.Sides.Light ) ? true : false );
	}

    public void OnWrongTap()
	{
		SfxManager.Instance.Play( SfxManager.SfxClip.Clip2, 1, C.PitchVal  );

		OnDestroy();

		//WorldController.Instance.DeductWorldRotation();
	}

    public C.Controls InputKey
    {
        set { m_inputKey = value; }
        get { return m_inputKey; }
    }

    public bool WasTap
    {
        set { m_bWasTap = value; }
        get { return m_bWasTap;  }
    }
}
