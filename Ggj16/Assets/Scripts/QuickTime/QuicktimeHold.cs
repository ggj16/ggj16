﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class QuicktimeHold : QuicktimeObject {
	
	[SerializeField] private Image RadTimer = null;

	public C.Controls m_inputKey1 = C.Controls.Invalid;
	public C.Controls m_inputKey2 = C.Controls.Invalid;

	private bool m_bWasHold = false;
	// Use this for initialization
	void Start () {
		//OnSpawn(C.Controls.Up, C.Sides.None, 10f);
	}

	protected override void UpdateObj ()
	{
		if(m_inputKey1 == C.Controls.Invalid || m_inputKey2 == C.Controls.Invalid)
			return;

		/*for(int i = 0; i < 8; i++)
		{
			for(int j = 0; j < 8; j++)
			{
				if(Input.GetButton(m_inputKeys[i]) && Input.GetButton(m_inputKeys[j]) )
				{
					if(i == (int)m_inputKey1 || j == (int)m_inputKey2){
						if(i == (int)m_inputKey1 && j == (int)m_inputKey2)
						{
							RadTimer.fillAmount += Time.deltaTime*C.DefaultHoldSpeed;
							if(RadTimer.fillAmount >= 1){
								OnHoldDone();
							}
						}
						else
						{
							Debug.Log("Wrong Hold");//OnWrongTap();
						}
					}
					else{
						
						RadTimer.fillAmount = 0f;
					}
				}
			}
		}*/

		if(Input.GetButton(m_inputKeys[(int)m_inputKey1]) && Input.GetButton(m_inputKeys[(int)m_inputKey2]))
		{
			RadTimer.fillAmount += Time.deltaTime*C.DefaultHoldSpeed;
			if(RadTimer.fillAmount >= 1 && !m_bWasHold){
				OnHoldDone();
			}
		}
		else
			RadTimer.fillAmount = 0f;

		base.UpdateObj ();
	}

	public override void OnSpawn(C.Controls p_input, C.Sides p_side, float p_time)
	{
		m_offset = 4;

		m_currSpr = m_symbol.sprite;

		m_inputKey1 = p_input;
		m_inputKey2 = p_input + m_offset;

		base.OnSpawn(p_input, p_side, p_time);

		RadTimer.transform.Rotate (Vector3.forward * (m_symbol.transform.rotation.z - m_rotation[(int)p_input]));
	}

	protected override void OnDestroy ()
	{
		iTween.Stop(gameObject);

		base.OnDestroy ();
	}

	private void OnHoldDone()
	{
		m_bWasHold = true;

		SfxManager.Instance.Play( SfxManager.SfxClip.Clip1, 1, C.PitchVal );

		OnAnimSuccess();

		WorldController.Instance.AddWorldRotation(2);
	}

	public override void OnAnimSuccess ()
	{
		base.OnAnimSuccess ();

		iTween.ValueTo( RadTimer.gameObject,
			iTween.Hash( "from", 1f, "to", 0f, "time", 0.35f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenRad", "onupdatetarget", gameObject));
	}

	private void TweenRad( float p_value )
	{
		RadTimer.color = new Color( 1,1,1, p_value );
	}
}
