﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class QuicktimeObject : MonoInstance<QuicktimeObject> {

	public Image m_symbol;

	private float m_timeToTap = 3f;
	public Sprite m_currSpr = null;
	public int m_offset = 0;

	public bool m_bIsToBeDestroyed = false;

	public string[] m_inputKeys = {C.P1Up, C.P1Down, C.P1Left, C.P1Right,
		C.P2Up, C.P2Down, C.P2Left, C.P2Right};

	public int[] m_rotation = {0,180,90,270};

	public float TimeToTap{
		set{m_timeToTap = value;}
		get{return m_timeToTap;}
	}

	protected override void Awake ()
	{

		base.Awake ();
	}

	protected override void OnDestroy ()
	{
		Destroy(gameObject);

		base.OnDestroy ();
	}

	void Update()
	{
		UpdateObj();
	}

	// Update is called once per frame
	protected virtual void UpdateObj () {
		//Timer
		/*RadTimer.fillAmount += Time.deltaTime*TimeToTap;
		Debug.Log(Time.deltaTime*TimeToTap);
		if(RadTimer.fillAmount >= 1){
			TimerDone();
		}*/
	}

	public virtual void OnSpawn(C.Controls p_input, C.Sides p_side, float p_time)
	{
		switch(p_input)
		{
		case C.Controls.Up:
			RotateSymbol(C.Controls.Up);
			break;
		case C.Controls.Down:
			RotateSymbol(C.Controls.Down);
			break;
		case C.Controls.Left:
			RotateSymbol(C.Controls.Left);
			break;
		case C.Controls.Right:
			RotateSymbol(C.Controls.Right);
			break;
		}

		if(m_symbol)
			m_symbol.sprite = m_currSpr;

		iTween.Stop(gameObject);
		iTween.ScaleTo ( gameObject, iTween.Hash ("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", p_time, "delay", 0, 
			"onCompleteTarget", gameObject, "onComplete","OnDestroyFade", "easeType", iTween.EaseType.easeInQuad) );
	}

	public virtual void OnDestroyFade()
	{
		SfxManager.Instance.Play( SfxManager.SfxClip.Clip2, 1, C.PitchVal  );

		WorldController.Instance.DeductWorldRotation();

		OnDestroy ();
	}

	public virtual void OnAnimSuccess()
	{
		if (m_bIsToBeDestroyed)
			return;

		m_bIsToBeDestroyed = true;
		
		iTween.Stop(m_symbol.gameObject);
		iTween.Stop(gameObject);
		iTween.ScaleTo ( m_symbol.gameObject, iTween.Hash ("scale", Vector3.one * 25f, "time", 0.5f, "easeType", iTween.EaseType.easeInQuad) );
		iTween.ValueTo( m_symbol.gameObject,
			iTween.Hash( "from", 1f, "to", 0f, "time", 0.35f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenTop", "onupdatetarget", gameObject,"onCompleteTarget", gameObject, "onComplete", "OnDestroy" ));
	}

	private void TweenTop( float p_value )
	{
		m_symbol.color = new Color( 1,1,1, p_value );
	}

	public void RotateSymbol(C.Controls p_dir)
	{
		if(m_symbol)
			m_symbol.transform.Rotate (Vector3.forward * m_rotation[(int)p_dir]);
	}
}
