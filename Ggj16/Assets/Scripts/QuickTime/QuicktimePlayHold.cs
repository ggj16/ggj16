﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class QuicktimePlayHold : QuicktimeObject {
	
	[SerializeField] private Image RadTimerRight = null;
	[SerializeField] private Image RadTimerLeft = null;
	[SerializeField] private Image Title = null;
	[SerializeField] private Image Tuts = null;
	[SerializeField] private Image PressAny = null;
	// no time to optimize!
	[SerializeField] private Image LeftArrow = null;
	[SerializeField] private Image RightArrow = null;

	public C.Controls m_inputKey1 = C.Controls.Right;
	public C.Controls m_inputKey2 = C.Controls.Right;

	private bool m_bWasStarted = false;

	private bool m_enableBtn = false;

	public bool EnableBtn{
		set{m_enableBtn = value;}
		get{return m_enableBtn;}
	}

	// Use this for initialization
	void Start () {
		OnSetup(C.Controls.Right, C.Sides.None, 10f);

		if( Title != null ){
			AnimateTitle();
		}
	}

	private void AnimateTitle()
	{
		iTween.RotateBy( Title.gameObject,
			iTween.Hash( "z", -0.5f, "time", 1.5f, "easeType", iTween.EaseType.easeOutQuint, "delay", 3f, "oncompletetarget", gameObject, "oncomplete", "AnimateTitle" ));		
	}

	protected override void UpdateObj ()
	{
		if(!EnableBtn)
			return;

		if(Input.GetButton(m_inputKeys[(int)m_inputKey2]))
		{
			if(RadTimerRight.fillAmount <= 0.5f)
				RadTimerRight.fillAmount += Time.deltaTime*C.DefaultHoldSpeed;
		}
		else
			RadTimerRight.fillAmount = 0f;

		if(Input.GetButton(m_inputKeys[(int)m_inputKey1]))
		{
			if(RadTimerLeft.fillAmount <= 0.5f)
				RadTimerLeft.fillAmount += Time.deltaTime*C.DefaultHoldSpeed;
		}
		else
			RadTimerLeft.fillAmount = 0f;

		if(RadTimerRight.fillAmount >= 0.5 && RadTimerLeft.fillAmount >= 0.5 && !m_bWasStarted){
			OnHoldDone();
		}

		base.UpdateObj ();
	}

	void OnSetup(C.Controls p_input, C.Sides p_side, float p_time)
	{
		m_offset = 4;

		m_currSpr = m_symbol.sprite;

		m_inputKey1 = p_input;
		m_inputKey2 = p_input + m_offset;
	}

	protected override void OnDestroy ()
	{
		iTween.Stop(gameObject);

		base.OnDestroy ();
	}

	private void OnHoldDone()
	{
		m_bWasStarted = true;

		SfxManager.Instance.Play( SfxManager.SfxClip.Clip1, 1, C.PitchVal  );

		OnAnimSuccess();

		iTween.ValueTo( m_symbol.gameObject,
			iTween.Hash( "from", 1f, "to", 0f, "time", 0.35f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenPlayFade", "onupdatetarget", gameObject));

		iTween.ValueTo( Tuts.gameObject,
			iTween.Hash( "from", 1f, "to", 0f, "time", 0.35f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenTutFade", "onupdatetarget", gameObject));

		iTween.ValueTo( RightArrow.gameObject,
			iTween.Hash( "from", 1f, "to", 0f, "time", 1f, "delay", .5f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenArrow", "onupdatetarget", gameObject));

		iTween.ValueTo( LeftArrow.gameObject,
			iTween.Hash( "from", 1f, "to", 0f, "time", 1f, "delay", .5f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenArrow", "onupdatetarget", gameObject));

		Invoke("GameStart", 0.35f);
	}

	void GameStart()
	{
		GameController.Instance.OnClickPlay();
	}

	private void TweenTitleFade( float p_value )
	{
		Title.color = new Color( 1,1,1, p_value );
	}

	private void TweenPlayFade( float p_value )
	{
		m_symbol.color = new Color( 1,1,1, p_value );
	}

	private void TweenTutFade( float p_value )
	{
		Tuts.color = new Color( 1,1,1, p_value );
	}

	private void TweenPressAnyFade( float p_value )
	{
		PressAny.color = new Color( 1,1,1, p_value );
	}

	public void ShowObject()
	{
		iTween.Stop( Title.gameObject );

		iTween.ValueTo( Title.gameObject,
			iTween.Hash( "from", 1f, "to", 0f, "time", 1f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenTitleFade", "onupdatetarget", gameObject));

		iTween.ScaleTo( Title.gameObject,
			iTween.Hash( "scale", Vector3.one * 2, "time", 1f, "easeType", iTween.EaseType.easeOutExpo ));

		iTween.Stop(PressAny.gameObject);
		iTween.ValueTo( PressAny.gameObject,
			iTween.Hash( "from", 1f, "to", 0f, "time", 1f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenPressAnyFade", "onupdatetarget", gameObject));

		iTween.ValueTo( m_symbol.gameObject,
			iTween.Hash( "from", 0f, "to", 1f, "time", 1f, "delay", .5f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenPlayFade", "onupdatetarget", gameObject));

		iTween.ValueTo( Tuts.gameObject,
			iTween.Hash( "from", 0f, "to", 1f, "time", 1f, "delay", .5f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenTutFade", "onupdatetarget", gameObject));
		
		iTween.ValueTo( RightArrow.gameObject,
			iTween.Hash( "from", 0f, "to", 1f, "time", 1f, "delay", .5f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenArrow", "onupdatetarget", gameObject));

		iTween.ValueTo( LeftArrow.gameObject,
			iTween.Hash( "from", 0f, "to", 1f, "time", 1f, "delay", .5f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenArrow", "onupdatetarget", gameObject));

		iTween.ValueTo( RightArrow.gameObject,
			iTween.Hash( "from", 60f, "to", 30f, "loopType", iTween.LoopType.pingPong, "time", 1f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenArrowDown", "onupdatetarget", gameObject));

		iTween.ValueTo( LeftArrow.gameObject,
			iTween.Hash( "from", 60f, "to", 30f, "loopType", iTween.LoopType.pingPong, "time", 1f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenArrowDown", "onupdatetarget", gameObject));

		Invoke ("EnableRad", 1f);
	}

	private void EnableRad()
	{
		EnableBtn = true;
	}

	private void TweenArrowDown(float p_value)
	{
		RightArrow.transform.localPosition = new Vector3(145, p_value, 0);
		LeftArrow.transform.localPosition = new Vector3(-145, p_value, 0);
	}

	public void AnimatePressAny()
	{
		iTween.ValueTo( PressAny.gameObject,
			iTween.Hash( "from", 1f, "to", 0f, "time", 1f, "loopType", iTween.LoopType.pingPong, "easeType", iTween.EaseType.easeInOutSine,
				"onupdate", "TweenPressAnyFade", "onupdatetarget", gameObject));
	}

	public override void OnAnimSuccess ()
	{
		base.OnAnimSuccess ();

		iTween.ValueTo( RadTimerRight.gameObject,
			iTween.Hash( "from", 1f, "to", 0f, "time", 0.35f, "easeType", iTween.EaseType.easeOutExpo,
				"onupdate", "TweenRad", "onupdatetarget", gameObject));
	}

	private void TweenRad( float p_value )
	{
		RadTimerRight.color = new Color( 1,1,1, p_value );
		RadTimerLeft.color = new Color( 1,1,1, p_value );
	}

	private void TweenArrow( float p_value )
	{
		LeftArrow.color = new Color( 1,1,1, p_value );
		RightArrow.color = new Color( 1,1,1, p_value );
	}
}
