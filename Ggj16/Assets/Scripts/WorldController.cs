﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class WorldController : MonoInstance<WorldController> {

	[SerializeField] private Transform m_tWorld = null;
	[SerializeField] private SpriteRenderer m_spriteNewWorld = null;

	[SerializeField] private EntityController[] m_entities = null;

	private float m_rotation = 0;

	private float ROTATION_ADDITIVE = 20f;
	private float ROTATION_SPEED = 3f;

	protected override void Awake()
	{
		base.Awake();
	}

	public void AddWorldRotation(int p_val = 1)
	{
		if( !DayController.Instance.InTransit ) {
			return;
		}

		m_rotation -= ROTATION_ADDITIVE * p_val;

		if( m_rotation < -360f ) {
			m_rotation = 0;

			DayController.Instance.EndDay();

			iTween.PunchScale( m_tWorld.gameObject, new Vector3( 1f, 1f, 0 ) * 0.25f, 1f );
		}		
	}

	public void DeductWorldRotation()
	{
		if( !DayController.Instance.InTransit ) {
			return;
		}

		m_rotation += ROTATION_ADDITIVE;

		if( m_rotation > 0 ) {
			m_rotation = 0;
		}

		iTween.PunchRotation( m_tWorld.gameObject, new Vector3( 0, 0, 5 ), 0.5f );
		//iTween.PunchPosition( m_tWorld.gameObject, new Vector3( 25, 25, 0 ), 0.35f );
	}

	public void ShakeWorld()
    {
        QuicktimeSpawner.Instance.DestroyAllSpawn();

        SfxManager.Instance.Play(SfxManager.SfxClip.Clip4);

		iTween.RotateBy( m_tWorld.gameObject,
			iTween.Hash( "z", 35f, "time", 2f, "loopType", iTween.LoopType.loop ));

		//iTween.ShakeRotation( m_tWorld.gameObject, new Vector3( 0, 0, 90 ), 2.0f );

		//iTween.PunchPosition( m_tWorld.gameObject, new Vector3( 25, 25, 0 ), 0.35f );

		iTween.ScaleTo( m_tWorld.gameObject,
			iTween.Hash( "scale", Vector3.one * 0, "time", 2f, "easeType", iTween.EaseType.easeInBack ));

        Invoke( "DelayedGameOver", 2f );
	}

	private void DelayedGameOver()
	{
		PlayerPrefs.SetInt( C.KeyGameSuccess, 0 );
        SceneManager.LoadScene( "EndScene" );
	}

	private void Update()
	{
		if( C.DEBUG ) {
			if( Input.GetKeyDown( KeyCode.V ) ) {
				AddWorldRotation();
			}
			if( Input.GetKeyDown( KeyCode.B ) ) {
				DeductWorldRotation();
			}
			if( Input.GetKeyDown( KeyCode.N ) ) {
				ShakeWorld();
			}
		}

		if( Input.GetKeyDown( KeyCode.P ) ) {
			SceneManager.LoadScene( "MainScene" );
		}

		m_tWorld.rotation = Quaternion.Lerp( m_tWorld.rotation, Quaternion.Euler( 0, 0, m_rotation ), Time.deltaTime * ROTATION_SPEED );
	}

	public void FadeInNewWorld()
	{
		SfxManager.Instance.Play(SfxManager.SfxClip.Clip3);

		Debug.Log( "NEW WORLD" );

		iTween.ValueTo( gameObject,
			iTween.Hash( "from", 0f, "to", 1f, "time", 4.0f, "easeType", iTween.EaseType.easeOutCubic,
				"onupdate", "TweenFade", "onupdatetarget", gameObject ));

		Invoke( "DelayedAnimOut", 8f );
	}
		
	public void DelayedAnimOut()
	{
		iTween.ScaleTo( m_tWorld.gameObject,
			iTween.Hash( "scale", Vector3.zero, "time", 1.0f, "easeType", iTween.EaseType.easeInBack ) );

		Invoke( "DelayedFinish", 1f );
	}

	public void DelayedFinish()
	{
		PlayerPrefs.SetInt( C.KeyGameSuccess, 1 );

		SceneManager.LoadScene( "EndScene" );		
	}

	public void TweenFade( float p_value )
	{
		m_spriteNewWorld.color = new Color( 1, 1, 1, p_value );
	}

	public void AnimateEntityIdle()
	{
		m_entities[ 0 ].AnimateIdle();
		m_entities[ 1 ].AnimateIdle();
	}

	public void AnimateEntityHappy( bool p_bSun )
	{
		m_entities[ p_bSun ? 0 : 1 ].AnimateHappy();
	}
}
