﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class EndController : MonoInstance<EndController> {

	[SerializeField] private GameObject m_grpCredits = null;
	[SerializeField] private GameObject m_grpQuotes = null;
	[SerializeField] private Text m_txtOver = null;

	protected override void Awake () {
		base.Awake();

		if( PlayerPrefs.GetInt( C.KeyGameSuccess ) > 0 ) {
			m_grpCredits.SetActive( true );
			m_grpQuotes.SetActive( true );

			m_txtOver.gameObject.SetActive( false );

			//OnAnimEnd call at animation
		}
		else {
			m_grpCredits.SetActive( false );
			m_grpQuotes.SetActive( false );
			GetComponent<Animator>().enabled = false;

			m_txtOver.gameObject.SetActive( true );

			Invoke( "OnAnimEnd", 3f );
		}
	}

	private void OnAnimEnd()
	{
		SceneManager.LoadScene( "MainScene" );
	}

}
