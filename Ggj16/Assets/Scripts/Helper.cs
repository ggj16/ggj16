﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Helper : MonoBehaviour
{
	public static int RandomDirection()
	{
		return Random.Range( 0, 2 ) == 1 ? 1 : -1;
	}

	public static bool RandomBoolean()
	{
		return System.Convert.ToBoolean( Random.Range( 0, 2 ) );
	}
	
	public static int GetDirectionFromBool( bool p_bValue )
	{
		return p_bValue ? 1 : -1;
	}

	public static Quaternion RandomRotation2D()
	{
		return Quaternion.Euler( Vector3.forward * Random.value * 360.0f );
	}

	public static Vector2 RandomNormalizeVector2D()
	{
		return RandomRotation2D() * Vector3.up;
	}

	public static List<T> Randomize<T>(List<T> list)
	{
		List<T> randomizedList = new List<T>();
		System.Random rnd = new System.Random();
		while( list.Count > 0 )
		{
			int index = rnd.Next( 0, list.Count ); //pick a random item from the master list
			randomizedList.Add( list[index] ); //place it at the end of the randomized list
			list.RemoveAt( index );
		}
		return randomizedList;
	}
}
