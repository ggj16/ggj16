﻿public static class C{

	public const bool DEBUG = false;

	//Player 1 Controls
	public const string P1Up = "P1-Up"; // 0
	public const string P1Down = "P1-Down"; // 1
	public const string P1Left = "P1-Left"; // 2
	public const string P1Right = "P1-Right"; // 3

	//Player 2 Controls
	public const string P2Up = "P2-Up"; // 4
	public const string P2Down = "P2-Down"; // 5
	public const string P2Left = "P2-Left"; // 6
	public const string P2Right = "P2-Right"; // 7

	public enum Controls
	{
		//--- Player 1
		Invalid = -1,
		Up = 0,
		Down = 1,
		Left = 2,
		Right = 3
	}

	public enum Sides
	{
		None,
		Light,
		Dark,
		gRey //Star wars?
	}

	public const float DefaultHoldSpeed = 0.7f;

	public const int DayMax = 8;

	public const string KeyGameSuccess = "keyGameSuccess";

	public const float PitchVal = 0.05f;
}