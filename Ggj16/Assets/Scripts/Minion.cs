﻿using UnityEngine;
using System.Collections;

public class Minion : MonoBehaviour {
	private void Awake () {
		GetComponent<Animator>().speed = 1 + Random.Range( -1f, 1f ) * 0.1f;
		GetComponent<SpriteRenderer>().flipX = Helper.RandomBoolean();
		transform.localScale = Vector3.one * Random.Range( 0.65f, 1f );
	}
}
